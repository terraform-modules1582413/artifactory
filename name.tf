module "name" {
  source = "git::git@repo.system4travel.com:iac/terraform-modules/name.git"

  prefix      = var.project_prefix
  environment = var.project_environment
  name        = "artifactory"
  resource    = "artifactory"
}
