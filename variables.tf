variable "project_prefix" {
  type = string
}
variable "project_environment" {
  type = string
}

variable "argocd_namespace" {
  type    = string
  default = "argocd"
}
variable "argocd_project" {
  type    = string
  default = "build"
}

variable "ingress_public_ip" {
  type = string
}
variable "ingress_class" {
  type = string
}

variable "cf_base_domain" {
  type = string
}

variable "artifactory_taints" {
  type = list(object({
    key    = string
    value  = string
    effect = string
  }))
  default = []
}
variable "artifactory_namespace" {
  type    = string
  default = "build"
}
variable "artifactory_repo_url" {
  type = string
}
variable "artifactory_crt_name" {
  type      = string
  sensitive = true
}
variable "artifactory_crt" {
  type      = string
  sensitive = true
}
variable "artifactory_crt_key" {
  type      = string
  sensitive = true
}
